from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)
import copy
import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


 
def replace_nth_char(str_arg, idx_replacement, char_replacement):
    return str_arg[0:idx_replacement] + char_replacement + str_arg[idx_replacement + 1:]

def update_memory_overflow(execution_step, blank_char):
    if (execution_step['position'] >= len(execution_step['memory'])):
        execution_step['memory'] += blank_char
    if (execution_step['position'] < 0):
        execution_step['memory'] = blank_char + execution_step['memory']
        execution_step['position'] = 0

def change_state(execution_step):
    if 'write' in execution_step['transition']:
        execution_step['memory'] = replace_nth_char(execution_step['memory'], execution_step['position'], execution_step['transition']['write']) 
    if 'L' in execution_step['transition']:
        execution_step['state'] = execution_step['transition']['L']
        execution_step['position'] = execution_step['position'] - 1
    else:
        execution_step['state'] = execution_step['transition']['R']
        execution_step['position'] = execution_step['position'] + 1
def update_position_only(execution_step):
    if (execution_step['transition'] == 'L'):
        execution_step['position'] -= 1
    else:
        execution_step['position'] += 1

def run_turing_machine(machine: Dict, input_: str, steps: Optional[int] = None,) -> Tuple[str, List, bool]:
    start_state = machine["start state"]
    execution_step = {'state': start_state, 'reading': input_[0],
                      'position': 0, 'memory': input_, 'transition':
                      machine["table"][start_state][input_[0]]}

    execution_history = [execution_step]

    while True:
        execution_step = copy.copy(execution_history[-1])
        print(execution_step)
        if type(execution_step['transition']) is dict:
            change_state(execution_step)
        else:
            update_position_only(execution_step)
        update_memory_overflow(execution_step, machine['blank'])
        pos = execution_step['position']
        char_read = execution_step['memory'][pos]
        if (execution_step['state'] != 'done'):
            execution_step['transition'] = machine['table'][execution_step['state']][char_read]
            execution_step['reading'] = char_read
        else:
            return ((execution_history[-1]['memory']).strip(machine['blank']), execution_history, True)
        execution_history.append(execution_step)
